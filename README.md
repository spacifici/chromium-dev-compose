## Usage

1. Create a `.env` file like the following in the project root folder:

  > ```sh
  > UUID=1000
  > UGID=1000
  > ```

2. Replace `UUID` value with your user id (`id -u`), the `UGID` value with your
user group id (`id -g`).

3. Synchronize the submodules with ```git submodule update --init```

4. Run: `docker compose up -d`.

5. Connect to the local ssh server: ```ssh -p 2222 user@localhost```

6. Change the default password (`password`). You will be disconnected after
   changing it.

7. Reconnect with ```ssh -p 2222 user@localhost```

8. Complete the **goma** login procedure.
